# Application part of repository

## Legal and copyright disclaimer

Sock Shop microservices demo application is developed and maintained by Weaveworks and Container Solutions.

You can see microservices demo application site and repositories:  
https://microservices-demo.github.io/  
https://github.com/microservices-demo

All source codes used exclusively for educational and non-profit goals.


## The Architecture

Application consists of 8 main and 7 additional (RabbitMQ, databases, NGINX, etc.) microservices deployed in Yandex Cloud Managed Service for Kubernetes in one HelmChart.

![Reference](/images/architecture.png)

The architecture of the demo microservices application was intentionally designed to provide as many microservices as possible. If you are considering your own design, we would recommend the iterative approach, whereby you only define new microservices when you see issues (performance/testing/coupling) developing in your application.

Furthermore, it is intentionally polyglot to exercise a number of different technologies. Again, we'd recommend that you only consider new technologies based upon a need.

As seen in the image above, the microservices are roughly defined by the function in an ECommerce site. Networks are specified, but due to technology limitations may not be implemented in some deployments.

All services communicate using REST over HTTP. This was chosen due to the simplicity of development and testing. Their API specifications are under development.


## Repository structure

```
application
├── .gitlab-ci - CI/CD manifests
├── carts - carts microservice
│   ├── api-spec - API specification
│   ├── src - source code for application
│   ├── test - source code for tests
│   ├── Dockerfile - Dockerfile to build microservice container image
│   ├── ... - some files need for go application build process
│   ...
```

## Building the Application

Each microservice is to be build as separate image in separate GitLab CI downstream pipeline.

![Reference](/images/pipeline.png)

Pipeline includes:
- container image build (Kanico Executor used to get advantages of layers caching and docker security)
- container image upload to Yandex Cloud Registry
- container image security scanning

![Reference](/images/container-scanning.png)

## Application Artifacts

Container images for every microservice can be pulled for containers to be started in Kubernetes cluster.
Gitlab Runner, Kubernetes cluster and Container Registry run in one cloud platform, Yandex Cloud.

![Reference](/images/registry.png)

Kubernetes apps stored as:

- Helm Charts in Infrastructure part of git repository [https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure/README.md)

- Flux CD HelmReleses custom resources manifests in GitOps parts of git repository [https://gitlab.com/voitenkov/sock-shop/-/tree/main/cluster/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/cluster/README.md)

## Applications documentation

For detailed documentation for each microservice pls see README.md file in the root of application folder:

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/carts/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/carts/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/catalogue/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/catalogue/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/front-end/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/front-end/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/orders/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/orders/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/payment/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/payment/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/queue-master/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/queue-master/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/shipping/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/shipping/README.md)

[https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/user/README.md](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps/user/README.md)


## Links to Other Chapters

See additional instructions and readme files:

- for infrastructure in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure](https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure) repository,

- for GitOps CD process in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/clusters](https://gitlab.com/voitenkov/sock-shop/-/tree/main/clusters) repository.

