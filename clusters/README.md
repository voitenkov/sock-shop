# GitOps part of repository

## Repository structure

```
clusters
├── production - production environment infrastructure
│   ├── additional - additional apps
│   ├── eso - External Secrets Operator manifests
│       ├── crs - external secrets custom resources manifests
│   ├── flux-system - Flux CD manifests (do not edit!)
│   ├── observability - observability apps
│       ├── dashboards - Grafana dashboards import manifests
│   ├── sock-shop - Sock Shop app
│       ├── crs - image update automation custom resources manifests
│   ...
```

## About GitOps CD Process

GitOps is a way of managing your infrastructure and applications so that whole system is described declaratively and version controlled (most likely in a Git repository), and having an automated process that ensures that the deployed environment matches the state specified in a repository.

For more information on GitOps, take a look at [What is GitOps?](https://www.gitops.tech/#what-is-gitops).

For main concepts of Flux CD pls see [https://fluxcd.io/flux/concepts/](https://fluxcd.io/flux/concepts/).

Flux CD implementation in this project provides following tasks:

1. Continious Deployment of applications to Kubernetes cluster. To deploy application with Flux CD we need to create Flux CD custom resorces, pointing to application Helm chart in **infrastructure/charts** folder of project repository. New application shall be deployed automatically by Flux CD.

2. Automated images update. Flux CD performs following operations:
    - scan the container registry and fetch the image tags
    - select the latest tag based on the defined policy (semver, calver, regex)
    - replace the tag in Kubernetes manifests (YAML format)
    - checkout a branch, commit and push the changes to the remote Git repository
    - apply the changes in-cluster and rollout the container image

To configure images updates Flux CD custom resorces pointing to images repositories and application charts were created.
You can see detailed info on Automated images update here: [https://fluxcd.io/flux/guides/image-update/](https://fluxcd.io/flux/guides/image-update/)


## External Secrets Operator (ESO)

External Secrets Operator (ESO) is a Kubernetes operator that integrates external secret management systems like AWS Secrets Manager, HashiCorp Vault and many more. The operator reads information from external APIs and automatically injects the values into a Kubernetes Secret.

In this project ESO works with Yandex Lockbox.

Detailed info on ESO: [https://external-secrets.io/main/](https://external-secrets.io/main/)

Yandex Lockbox: [https://cloud.yandex.ru/docs/lockbox/concepts/](https://cloud.yandex.ru/docs/lockbox/concepts/)

![Reference](/images/lockbox.png)

## Links to Other Chapters

See additional instructions and readme files:

- for application in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps) repository

- for infrastructure in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure](https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure) repository,

