# Legal and copyright disclaimer

Sock Shop microservices demo application is developed and maintained by Weaveworks and Container Solutions.

You can see microservices demo application site and repositories:  
https://microservices-demo.github.io/  
https://github.com/microservices-demo

All source codes used exclusively for educational and non-profit goals.

# About the Project

**Sock Shop** simulates the user-facing part of an e-commerce website that sells socks.  
It is intended to aid the demonstration and testing of microservice and cloud native technologies.  
This application was developed to demonstrate:
- examples of microservices running on Kubernetes platform,
- CI/CD process based on Gitlab CI, 
- DevOps principles such as IaaC (Infrastructure As A Code),
- DevOps (Terraform, Flux CD, etc.) and observability (Prometheus, Grafana, Loki) instruments.

![Reference](./images/sock-shop.png)

See additional instructions and readme files:

- for application in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps](https://gitlab.com/voitenkov/sock-shop/-/tree/main/apps) repository,

- for infrastructure in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure](https://gitlab.com/voitenkov/sock-shop/-/tree/main/infrastructure) repository,

- for GitOps CD process in [https://gitlab.com/voitenkov/sock-shop/-/tree/main/clusters](https://gitlab.com/voitenkov/sock-shop/-/tree/main/clusters) repository.


## Project Technological Stack

- Go kit - backend application
- Java Spring Boot - backend application
- Node.js - frontend application
- Mongo - services datastore
- MySQL - catalogue datastore
- Redis - services cache
- RabbitMQ - order queue
- NGINX - as an edge load balancer in some configurations.
- Docker containers
- Kubernetes orchestration
- Yandex Cloud:
  - project infrastructure
  - application artifacts
  - secrets, DNS, certificates, etc.


## Instruments Used in Project

- GitLab CI:
  - application and infrastructure source code
  - CI/CD process for application
  - CI/CD process for cloud infrastructure
- Terraform - deployment of development and production environments
- Flux CD - Kubernetes applications CD
- Prometheus - metrics collection for application and platform
- Grafana - observability for application and platform 
- Alertmanager - alerting on application critical issues
- Promtail/Loki - logs collection for application and platform
- Jaeger - application distributed tracing


## Contributing

Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a merge request. You can also simply open an issue with the tag "enhancement".

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request


## Contact

Andrey Voytenkov - avoytenkov@yandex.ru

Project link: [https://gitlab.com/voitenkov/sock-shop.git](https://gitlab.com/voitenkov/sock-shop.git)
