variable "folder_project" {
  type        = string
  description = "Name of project (cloud) folder belongs to"
}

variable "folder_environment" {
  type        = string
  description = "Name of environment related to folder"
}