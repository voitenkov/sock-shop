variable "sa_name" {
  type        = string
  description = "Service account name"
}

variable "sa_description" {
  type        = string
  description = "Service account description"
}

variable "sa_folder_id" {
  type        = string
  description = "Service account folder ID"
}

variable "sa_role" {
  type        = string
  description = "Service account role"
}