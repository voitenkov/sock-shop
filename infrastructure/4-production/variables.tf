variable "cloud_id" {
  type        = string
  description = "YC Cloud ID"
}

variable "folder_id" {
  type        = string
  description = "YC Folder ID"
}

variable "project" {
  type        = string
  description = "Name of project (cloud)"
}

variable "environment" {
  type        = string
  description = "Name of environment"
}

variable "domain1" {
  type        = string
  description = "Name of domain 1"
}

variable "subdomain1_1" {
  type        = string
  description = "Name of subdomain 1 of domain 1"
}

variable "runner_registration_token" {
  type        = string
  description = "Registration token for GitLab Runner"
}

variable "yc_alb_sa_secret_key" {
  type        = string
  description = "Secret Key for YC ALB Service Account"
}

variable "tg_bot_token" {
  type        = string
  description = "Telegram bot token for alerts"
}

variable "tg_chat_id" {
  type        = string
  description = "Telegram chat ID for alerts"
}

variable "yc_cr_login" {
  type        = string
  description = "Credentials to Yandex Container Registry"
}
